appAngular.controller("PagosITJO", function (
  $scope,
  $location,
  $http,
  configuracionGlobal,
  $routeParams
) {
  $scope.usuario1 = [
    {
      id: "02",
      nombre: "jose",
      apellido: "ramirez",
      correo: "jose@gmail.com",
      cedula: 123456,
      tlf: 5825161616,
    },
    {
      id: "03",
      nombre: "ramon",
      apellido: "ramiperezez",
      correo: "ramon@gmail.com",
      cedula: 789123,
      tlf: 64323343214,
    },
    {
      id: "04",
      nombre: "martin",
      apellido: "contreras",
      correo: "martin@gmail.com",
      cedula: 456789,
      tlf: 23452437990,
    },
  ];

  $scope.iniciarSesion = function () {
    for (let index = 0; index < 3; index++) {
      if ($scope.cedula == $scope.usuario1[index].cedula) {
        alert("hola");
        $location.path("/contacto/" + $scope.cedula);
      }
    }
  };

  $scope.agregarTarea = function () {
    if ($scope.nuevaTarea != null)
      $scope.usuario1.push({
        texto: $scope.nuevaTarea,
        cedula2: $scope.nuevaTarea2,
        apellido2: $scope.nuevaTarea4,
        tlf2: $scope.nuevaTarea5,
        correo2: $scope.nuevaTarea6,
      });
    $scope.nuevaTarea = null;
    $scope.nuevaTarea2 = null;
    $scope.nuevaTarea4 = null;
    $scope.nuevaTarea5 = null;
    $scope.nuevaTarea6 = null;
  };

  // Modelo que permite eliminar tarea
  $scope.eliminarTarea = function (dato) {
    var pos = $scope.usuario1.indexOf(dato);
    alert(pos);
    $scope.usuario1.splice(pos);
    $scope.nuevaTarea3 = null;
  };
});
